CREATE OR REPLACE PROCEDURE p_servicios_modelo(pin_nume_serv_desde IN servicios.nume_serv%TYPE
                                              ,pin_nume_serv_hasta IN servicios.nume_serv%TYPE
                                              ,pout_msg_err        OUT VARCHAR2) IS
  CURSOR c_servicios IS
    SELECT nume_serv
          ,pate_vehi_actu
          ,marc_vehi
          ,mode_vehi
          ,nume_radi
      FROM serviciossocios
     WHERE nume_serv BETWEEN pin_nume_serv_desde AND pin_nume_serv_hasta;
  v_nume_loca      localidades.nume_loca%TYPE;
  v_nomb_loca      localidades.nomb_loca%TYPE;
  v_nume_comp      companias.nume_comp%TYPE;
  v_nomb_comp      companias.nomb_comp%TYPE;
  v_error          VARCHAR2(20);
  v_fech_serv      servicios.fech_serv%TYPE;
  v_nume_tipo_serv servicios.nume_tipo_serv%TYPE;
  v_nomb_tipo_serv tiposservicios.nomb_tipo_serv%TYPE;
  v_commit         NUMBER;
  v_nume_base      bases.nume_base%TYPE;
  v_nomb_base      bases.nomb_base%TYPE;
BEGIN
  DELETE FROM t_servicios_modelos
   WHERE numero_servicio IS NOT NULL;
  v_commit := 0;
  FOR r IN c_servicios LOOP
    BEGIN
      v_error := 'serviciosdirecciones';
      SELECT nume_loca
        INTO v_nume_loca
        FROM serviciosdirecciones
       WHERE nume_serv = r.nume_serv
         AND rownum = 1;
      v_error := 'localidades';
      SELECT nomb_loca
        INTO v_nomb_loca
        FROM localidades
       WHERE nume_loca = v_nume_loca;
      v_error := 'companiasradios';
      SELECT nume_comp
        INTO v_nume_comp
        FROM companiasradios
       WHERE nume_radi = r.nume_radi;
      v_error := 'companias';
      SELECT nomb_comp
        INTO v_nomb_comp
        FROM companias
       WHERE nume_comp = v_nume_comp;
    
      SELECT fech_serv
            ,nume_tipo_serv
            ,nume_base
        INTO v_fech_serv
            ,v_nume_tipo_serv
            ,v_nume_base
        FROM servicios
       WHERE nume_serv = r.nume_serv;
    
      SELECT nomb_tipo_serv
        INTO v_nomb_tipo_serv
        FROM tiposservicios
       WHERE nume_tipo_serv = v_nume_tipo_serv;
       
       SELECT nomb_base
       INTO v_nomb_base
       FROM bases
       WHERE nume_base = v_nume_base;
    
      INSERT INTO t_servicios_modelos
        (numero_servicio
        ,patente
        ,marca
        ,modelo
        ,nume_loca
        ,localidad
        ,nume_comp
        ,nombre_compania
        ,tipo_de_servicio
        ,fecha
        ,numero_base
        ,nombre_base)
      VALUES
        (r.nume_serv
        ,r.pate_vehi_actu
        ,r.marc_vehi
        ,r.mode_vehi
        ,v_nume_loca
        ,v_nomb_loca
        ,v_nume_comp
        ,v_nomb_comp
        ,v_nomb_tipo_serv
        ,v_fech_serv
        ,v_nume_base
        ,v_nomb_base);
      IF v_commit = 5000 THEN
        COMMIT;
        v_commit := 0;
      ELSE
        v_commit := v_commit + 1;
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        pout_msg_err := ('Error en la tabla ' || v_error || ' ' || SQLERRM || 'para el servicio ' || r.nume_serv);
    END;
  END LOOP;
  COMMIT;
END p_servicios_modelo;
/
/
